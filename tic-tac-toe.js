var xzero,opp;
var answers = [];
var flag = false;
span = document.createElement("p");
span.style.color = "#FFF";
span.style.fontSize = "56px";
span.style.position = "absolute";
span.style.top = "40%";
span.style.left = "40%";
/*HELPER*/
function setText(id,text){
    document.querySelector(id).innerHTML = text;
}

function show(id){
    document.getElementById(id).style.display = 'block';
}

function hide(id){
    document.getElementById(id).style.display = 'none';
}
function check(id){
    if(document.getElementById(id).innerHTML === ""){
        h1 = document.createElement("h1");
        if(xzero === "X")
            h1.style.color = "#BEDB39";
        else if(xzero === "O")
            h1.style.color = "#FD7400";
        
            h1.textContent = xzero;
            h1.style.lineHeight = "100px";
            h1.style.margin = 0;
            h1.style.textAlign = "center";
            document.getElementById(id).appendChild(h1);
            answers.push(id);
        //because minimum 5 moves lagte hai to win 
        if(answers.length>4){
                checkConditions();
                if(flag)
                    return;
        }
        if(answers.length<9){
                    do{
                        position = 1+ Math.round(Math.random()*8);
                    }while(answers.indexOf(position)>-1);
                    h1 = document.createElement("h1");
                    if(xzero === "X"){
                        h1.textContent = "O";
                        h1.style.color = "#FD7400";
                    }
                    else if(xzero === "O"){
                        h1.textContent = "X";
                        h1.style.color = "#BEDB39";
                    }
                    h1.style.lineHeight = "100px";
                    h1.style.margin = 0;
                    h1.style.textAlign = "center";
                    document.querySelector(".box"+position).appendChild(h1);
                    answers.push(position);
                    checkConditions();
            }
            
        }
}
function checkConditions(){
    for(j=1;j<10;j+=3){
            if(document.getElementById(j).textContent==xzero && document.getElementById(j+1).textContent==xzero
            && document.getElementById(j+2).textContent==xzero){
                hide("all");
                span.textContent = xzero +" "+ "Wins!";
                document.querySelector(".main-container").appendChild(span);
                flag = true;
                break;

            }
            else if(document.getElementById(j).textContent==opp
            && document.getElementById(j+1).textContent==opp && document.getElementById(j+2).textContent==opp){
                hide("all");
                span.textContent = opp +" "+ "Wins!";
                document.querySelector(".main-container").
                appendChild(span);
                flag = true;
                break;
            }
        
    }
    if(flag)
        return;
    for(j=1;j<4;j++){
            if(document.getElementById(j).textContent ==xzero       && document.getElementById(j+3).textContent==xzero      && document.getElementById(j+6).textContent==xzero)      {
                hide("all");
                span.textContent = xzero +" "+ "Wins!";
                document.querySelector(".main-container").
                appendChild(span);
                flag = true;
                break;
        }
        else if(document.getElementById(j).textContent ==opp       && document.getElementById(j+3).textContent==opp && document.getElementById(j+6).textContent==opp){
                hide("all");
                span.textContent = opp +" "+ "Wins!";
                document.querySelector(".main-container").
                appendChild(span);
                flag = true;
                break
        }
    }
    if(flag)
        return;
    j=1;
    if(document.getElementById(j).textContent ==xzero &&  document.getElementById(j+4).textContent == xzero 
    && document.getElementById(j+8).textContent == xzero){
            hide("all");
            span.textContent = xzero +" "+ "Wins!";
            document.querySelector(".main-container").
            appendChild(span);
            flag = true;
            return;
        }
    else if(document.getElementById(j).textContent ==opp &&  document.getElementById(j+4).textContent == opp 
    && document.getElementById(j+8).textContent == opp){
            hide("all");
            span.textContent = opp +" "+ "Wins!";
            document.querySelector(".main-container").
            appendChild(span);
            flag = true;
            return;
    }
    j=3;
    if(document.getElementById(j).textContent ==xzero && document.getElementById(j+2).textContent == xzero
    &&  document.getElementById(j+4).textContent == xzero){
            hide("all");
            span.textContent = xzero +" "+ "Wins!";
            document.querySelector(".main-container").
            appendChild(span);
            flag = true;
            return;
    }

    else if(document.getElementById(j).textContent ==opp && document.getElementById(j+2).textContent == opp
    &&  document.getElementById(j+4).textContent == opp){
                hide("all");
                span.textContent = opp +" "+ "Wins!";
                document.querySelector(".main-container").
                appendChild(span);
                flag = true;
                return;
        }
    if(answers.length == 9 && !flag){
            span.textContent = xzero +opp + " "+ "Draw!";
            hide("all");
            document.querySelector(".main-container").appendChild(span);

    }
}

document.querySelector(".x").onclick = function(){
    document.querySelector(".zero").style.border = "none";
    this.style.border = "solid 3px #397EE5";
    xzero = this.innerHTML;
    opp = document.querySelector(".zero").innerHTML;
}
document.querySelector(".restart").onclick = function(){
    window.location.reload();
}
document.querySelector(".zero").onclick = function(){
    document.querySelector(".x").style.border = "none";
    this.style.border = "solid 3px #397EE5";
    xzero = this.innerHTML;
    opp = document.querySelector(".x").innerHTML;
    if(answers.length ==0){
                position = 1+ Math.round(Math.random()*8);
                h1 = document.createElement("h1");
                if(xzero === "X"){
                        h1.textContent = "O";
                        h1.style.color = "#FD7400";
                }
                else{
                    h1.textContent = "X";
                    h1.style.color = "#BEDB39";
                }
                h1.style.lineHeight = "100px";
                h1.style.margin = 0;
                h1.style.textAlign = "center";
                document.querySelector(".box"+position).appendChild(h1);
                answers.push(position);
    }
}

document.querySelector(".box1").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box2").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box3").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box4").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box5").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box6").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box7").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box8").onclick = function(e){
        check(parseInt(e.target.id));
}
document.querySelector(".box9").onclick = function(e){
        check(parseInt(e.target.id));
}